#!/bin/bash

set -eo pipefail

MESA_VERSION=20.0.6

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y install \
    curl \
    git \
    clang-10 \
    cmake \
    pkg-config \
    libtool-bin \
    meson \
    ninja-build  \
    python3-pip \
    libx11-dev \
    libxext-dev \
    x11proto-core-dev \
    x11proto-gl-dev \
    libglew-dev \
    freeglut3-dev \
    bison \
    flex

pip3 install \
    mako \
    lxml

curl -O https://mesa.freedesktop.org/archive/mesa-${MESA_VERSION}.tar.xz
tar xf mesa-${MESA_VERSION}.tar.xz
cd mesa-${MESA_VERSION}
mkdir -p build

meson --buildtype=release \
        -Dglx=gallium-xlib \
        -Dvulkan-drivers= \
        -Ddri-drivers= \
        -Dgallium-drivers=swrast,swr \
        -Dplatforms=x11 \
        -Dgallium-omx=disabled \
        -Dprefix=/home/openswr/.local \
        -Dosmesa=gallium \
        build

ninja -C build

meson install -C build
